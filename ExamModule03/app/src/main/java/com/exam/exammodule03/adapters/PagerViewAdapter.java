package com.exam.exammodule03.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.exam.exammodule03.fragments.BaseFragment;
import com.exam.exammodule03.fragments.FragmentTab1;
import com.exam.exammodule03.fragments.FragmentTab2;

/**
 * Created by PC14-02 on 12/4/2015.
 */
public class PagerViewAdapter extends FragmentPagerAdapter {

    BaseFragment[] fragments = new BaseFragment[2];

    public PagerViewAdapter(FragmentManager fm) {
        super(fm);

        fragments[0] = new FragmentTab1();
        fragments[1] = new FragmentTab2();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
