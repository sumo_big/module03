package com.exam.exammodule03.fragments;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.exam.exammodule03.R;
import com.exam.exammodule03.adapters.HocsinhAdapter;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentTab1 extends BaseFragment{

    ListView listView;
    HocsinhAdapter adapter;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_tab1;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new HocsinhAdapter(getActivity());
        listView.setAdapter(adapter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
