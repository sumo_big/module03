package com.exam.exammodule03.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;

import com.exam.exammodule03.R;
import com.exam.exammodule03.database.HocsinhDateBase;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class SQLiteFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        String sql ="select * from lophoc";
        return R.layout.fragment_sqlite;
    }

    @Override
    public void initView(View view) {
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);
        View btn4 = view.findViewById(R.id.button4);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Them lop hoc");
                themLophoc();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Hien thi danh sach lop hoc");
                hienthidanhsachhocsinh();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

    }

    private void themLophoc(){
        Random random = new Random();
        int a = random.nextInt(10) + 1;
        String tenlop = "Lop" + a;
        int namhoc = 2015;

        HocsinhDateBase dateBase = new HocsinhDateBase(getActivity());
        SQLiteDatabase db = dateBase.getWritableDatabase();
        //SQLiteDatabase db = dateBase.getReadableDatabase();

        //c1
        /*String sql = "insert into lophoc(tenlop, namhoc) values('" + tenlop + "', " + namhoc +")";
        //insert into lophoc(tenlop, namhoc) values("lop3c", 2015);
        System.out.println("sql: " + sql);
        db.execSQL(sql);*/

        //c2
        ContentValues values = new ContentValues();
        values.put("tenlop", tenlop);
        values.put("namhoc", namhoc);

        long id = db.insert("lophoc", null, values);
        System.out.println("row id - " + id);


    }

    private void hienthidanhsachhocsinh(){
        HocsinhDateBase dateBase = new HocsinhDateBase(getActivity());
        SQLiteDatabase db = dateBase.getReadableDatabase();

        // where malop < 2
        /*String sql = "select * from lophoc where malop < ?";
        String agrs[] = new String[]{"3"};
        Cursor cs = db.rawQuery(sql, agrs);*/

        String sql = "select * from lophoc";
        Cursor cs = db.rawQuery(sql, null);

        if(cs!=null /*&& cs.getCount()>0*/){
            int n = cs.getCount();
            System.out.println("count " + n);
            if(!cs.moveToFirst())
                return;

            ArrayList<String> arr = new ArrayList<>();

            do{

                int idxML = cs.getColumnIndex("malop");
                int idxTL = cs.getColumnIndex("tenlop");
                int idxNH = cs.getColumnIndex("namhoc");

                int id = cs.getInt(idxML);
                String tenlop = cs.getString(idxTL);
                int namhoc = cs.getInt(idxNH);

                arr.add(tenlop);

                System.out.println(id + " - " + tenlop + " - " + namhoc);

            }while (cs.moveToNext());

            //set array data to adapter.

        }

    }

    private void update(){
        HocsinhDateBase dateBase = new HocsinhDateBase(getActivity());
        SQLiteDatabase db = dateBase.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put("namhoc", 2016);

        long id = db.update("lophoc", values, "malop = ?", new String[]{"7"});
        System.out.println("update " + id);

    }

    private void delete(){
        HocsinhDateBase dateBase = new HocsinhDateBase(getActivity());
        SQLiteDatabase db = dateBase.getReadableDatabase();

        long id = db.delete("lophoc", "malop = ?", new String[]{"6"});
        System.out.println("update " + id);
    }

}
