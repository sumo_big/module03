package com.exam.exammodule03.fragments;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.widget.TextView;

import com.exam.exammodule03.MainActivity;
import com.exam.exammodule03.R;

import java.util.Random;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentNotification extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_notification;
    }

    @Override
    public void initView(View view) {

        //
        View btn = view.findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postNotify();
            }
        });



    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void postNotify(){

        Random random = new Random(System.currentTimeMillis());
        int id = random.nextInt();

        //intent giong nhu tao intent de chuyen man hinh.
        Intent intent = new Intent(getActivity(), MainActivity.class);

        //truyen du lieu
        Bundle bundle = new Bundle();
        bundle.putString("data1", "" + random.nextInt());

        intent.putExtras(bundle);

        //
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //set thong tin hien thi
        Notification.Builder builder = new Notification.Builder(getActivity());
        builder.setContentTitle("title abc");
        builder.setContentText("day la noi dung thong bao " + random.nextInt());
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.owl);
        builder.setSound(sound);

        //builder.setSound(null);
        builder.setContentIntent(pendingIntent);

        //dung de post notify len he thong
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(id, builder.build());

    }

}
