package com.exam.exammodule03.fragments;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.exam.exammodule03.R;

import java.util.ArrayList;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class MenuContextFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_menu_context;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        View btn1 = view.findViewById(R.id.button1);
        ListView lview = (ListView) view.findViewById(R.id.listView);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("MenuContext");

            }
        });

        Bundle bundle = getArguments();
        if(bundle!=null){
            System.out.println(bundle.getString("name"));
            System.out.println(bundle.getInt("id"));
        }

        //dang ky su kien long tren button
        registerForContextMenu(btn1);

        //show listview
        ArrayList<String>data = new ArrayList<>();
        for (int i=0; i<20; i++){
            data.add("item " + i);
        }

        //
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
        lview.setAdapter(adapter);

        //
        registerForContextMenu(lview);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.button1){
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_context, menu);
        }
        else{
            AdapterView.AdapterContextMenuInfo adapterInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
            System.out.println("long item index " + adapterInfo.position);
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_context2, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        System.out.println("item click");

        return true;
    }
}
