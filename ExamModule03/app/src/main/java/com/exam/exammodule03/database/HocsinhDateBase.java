package com.exam.exammodule03.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PC14-02 on 11/27/2015.
 */
public class HocsinhDateBase extends SQLiteOpenHelper{
    private static final String DATABASENAME = "QLHS";
    private static int DATABAVERSION = 2;

    public static final String TABLE_LOPHOC = "lophoc";

    /*
    * version 1
    * QL lop hoc
    * */

    /*
    * version 2
    * QL them hoc sinh
    * */

    public HocsinhDateBase(Context context) {
        super(context, DATABASENAME, null, DATABAVERSION);
        System.out.println("constructor db - version " + DATABAVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("onCreate DB");
        //v1 => them table lophoc
        String sqlCreatTableLophoc = "CREATE TABLE [" + TABLE_LOPHOC + "] (malop integer NOT NULL PRIMARY KEY AUTOINCREMENT,tenlop text NOT NULL,namhoc integer NOT NULL)";
        db.execSQL(sqlCreatTableLophoc);
        //v2.
        String sqlCreateTableHocsinh = "CREATE TABLE [hocsinh] (id integer NOT NULL PRIMARY KEY AUTOINCREMENT,tenhs text NOT NULL,diachi text,malop integer)";
        db.execSQL(sqlCreateTableHocsinh);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("onUpgrade DB - oldVer(" + oldVersion + "), newVer(" + newVersion + ")");

        switch (oldVersion){
            case 1:
            case 2:
                //them table hocsinh
                //v2.
                String sqlCreateTableHocsinh = "CREATE TABLE [hocsinh] (id integer NOT NULL PRIMARY KEY AUTOINCREMENT,tenhs text NOT NULL,diachi text,malop integer)";
                db.execSQL(sqlCreateTableHocsinh);
            case 3:
                //add column "gvcn"
                //ALTER TABLE lophoc ADD COLUMN 'gvcn' text
            case 4:
                //add column "hocsinh"
                break;
        }

    }
}
