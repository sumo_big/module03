package com.exam.exammodule03.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 12/4/2015.
 */
public class HocsinhAdapter extends BaseAdapter{

    private Context context;

    public HocsinhAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        HolderView holder = null;
        if(view==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_item_hocsinh, parent, false);

            holder = new HolderView(view);

            //
            view.setTag(holder);
        }
        else{
            holder = (HolderView) view.getTag();
        }
        
        String ten = "Nguyen van " + position;
        String ngaysinh = "" + position;
        String diem = position + "";



        holder.tenTV.setText(ten);
        holder.ngaysinhTV.setText(ngaysinh);
        holder.diemTV.setText(diem);

        return view;
    }

    private class HolderView{

        TextView tenTV;
        TextView ngaysinhTV;
        TextView diemTV;

        public HolderView(View view) {
            tenTV = (TextView) view.findViewById(R.id.textView1);
             ngaysinhTV = (TextView) view.findViewById(R.id.textView2);
             diemTV = (TextView) view.findViewById(R.id.textView3);
        }
    }

}
