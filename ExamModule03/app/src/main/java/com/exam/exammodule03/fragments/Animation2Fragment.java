package com.exam.exammodule03.fragments;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class Animation2Fragment extends BaseFragment{

    ImageView img, img2;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_animation2;
    }

    @Override
    public void initView(View view) {
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);
        View btn4 = view.findViewById(R.id.button4);
        View btn5 = view.findViewById(R.id.button5);

        img = (ImageView) view.findViewById(R.id.imageView);
        img2 = (ImageView) view.findViewById(R.id.imageView2);

        btn1.setOnClickListener(onclick);
        btn2.setOnClickListener(onclick);
        btn3.setOnClickListener(onclick);
        btn4.setOnClickListener(onclick);
        btn5.setOnClickListener(onclick);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click");
            }
        });

        //start animation drawable
        AnimationDrawable anim = (AnimationDrawable) img2.getDrawable();
        anim.start();
    }

    View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button1:
                    alpha();
                    break;
                case R.id.button2:
                    scale();
                    break;
                case R.id.button3:
                    translate();
                    break;
                case R.id.button4:
                    rotate();
                    break;
                case R.id.button5:
                    scaleRotate();
                    break;
            }
        }
    };

    private void alpha(){
        ObjectAnimator alpha = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.alpha);
        alpha.setTarget(img);
        alpha.start();
    }

    private void scale(){
        AnimatorSet scale = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.scale);
        scale.setTarget(img);
        scale.start();
    }

    private void translate(){
        AnimatorSet translate = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate);
        translate.setTarget(img);
        translate.start();
    }

    private void rotate(){
        AnimatorSet rotate = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.rotation);
        rotate.setTarget(img);
        rotate.start();
    }

    private void scaleRotate(){
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate_rotate);
        set.setTarget(img);
        set.start();
    }
}
