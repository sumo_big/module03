package com.exam.exammodule03.constanst;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public enum FragmentId {

    HOME("home"),
    ANIMATION("animation"),
    ANIMATION2("animation2"),
    SQLITE("sqlite"),
    MENU("menu"),
    MENU_FLOAT_CONTEXT("menu_float"),
    MENU_ACTIONMODE("menu_actionmode"),
    ACTIONBAR("actionbar"),
    ACTIONBAR_DROPDOWN("actionbar_dropdown"),
    ACTIONBAR_TABS("actionbar_tabs"),
    PAGERVIEW("pagerview"),
    TABS_PAGERVIEW("tab_pagerview"),
    CONTENTPROVIDER("CONTENTPROVIDER"),
    THREAD("THREAD"),
    NOT_SER_BRO("NOT_SER_BRO"),
    BROADCASTRECEIVER("BROADCAST"),
    NOTIFICATION("NOTIFICATION"),
    SERVICE("SERVICE");

    private String key;

    private FragmentId(String key){
        this.key = key;
    }

    public String getKey(){
        return key;
    }

}
