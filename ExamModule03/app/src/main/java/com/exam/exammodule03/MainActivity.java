package com.exam.exammodule03;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.exam.exammodule03.constanst.FragmentId;
import com.exam.exammodule03.fragments.ActionbarDropdownFragment;
import com.exam.exammodule03.fragments.ActionbarFragment;
import com.exam.exammodule03.fragments.ActionbarTabsFragment;
import com.exam.exammodule03.fragments.Animation2Fragment;
import com.exam.exammodule03.fragments.AnimationFragment;
import com.exam.exammodule03.fragments.BaseFragment;
import com.exam.exammodule03.fragments.FragmentBroadcastReceiver;
import com.exam.exammodule03.fragments.FragmentContentProvider;
import com.exam.exammodule03.fragments.FragmentNotification;
import com.exam.exammodule03.fragments.FragmentNotification_Broadcast_Service;
import com.exam.exammodule03.fragments.FragmentPagerView;
import com.exam.exammodule03.fragments.FragmentService;
import com.exam.exammodule03.fragments.FragmentThread;
import com.exam.exammodule03.fragments.HomeFragment;
import com.exam.exammodule03.fragments.MenuActionModeFragment;
import com.exam.exammodule03.fragments.MenuContextFragment;
import com.exam.exammodule03.fragments.MenuFragment;
import com.exam.exammodule03.fragments.SQLiteFragment;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        boolean notificaton = false;
        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                String key = bundle.getString("data1");
                System.out.println("data1:" + key);
                if(key!=null){
                    notificaton = true;
                }
            }
        }

        //
        if(notificaton){
            addFragment(FragmentId.NOTIFICATION, null, false);
        }
        else {
            addFragment(FragmentId.HOME, null, false);
        }
        //ktra db co ton tai hay chua? -> copy vao local



    }

    @Override
    protected void onNewIntent(Intent intent) {

        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                String key = bundle.getString("data1");
                System.out.println("data1:" + key);
            }
        }

    }

    public void addFragment(FragmentId id, Bundle bundle, boolean addToStack){

        BaseFragment fragment = null;
        if(id == FragmentId.HOME){
            fragment = new HomeFragment();
        }
        else if(id==FragmentId.ANIMATION){
            fragment = new AnimationFragment();
        }
        else if(id==FragmentId.ANIMATION2){
            fragment = new Animation2Fragment();
        }
        else if(id==FragmentId.SQLITE){
            fragment = new SQLiteFragment();
        }
        else if(id==FragmentId.MENU){
            fragment = new MenuFragment();
        }
        else if(id==FragmentId.MENU_FLOAT_CONTEXT){
            fragment = new MenuContextFragment();
        }
        else if(id==FragmentId.MENU_ACTIONMODE){
            fragment = new MenuActionModeFragment();
        }
        else if(id==FragmentId.ACTIONBAR){
            fragment = new ActionbarFragment();
        }
        else if(id==FragmentId.ACTIONBAR_DROPDOWN){
            fragment = new ActionbarDropdownFragment();
        }
        else if(id==FragmentId.ACTIONBAR_TABS){
            fragment = new ActionbarTabsFragment();
        }
        else if(id==FragmentId.PAGERVIEW){
            fragment = new FragmentPagerView();
        }
        else if(id==FragmentId.CONTENTPROVIDER){
            fragment = new FragmentContentProvider();
        }
        else if(id==FragmentId.THREAD){
            fragment = new FragmentThread();
        }
        else if(id==FragmentId.NOT_SER_BRO){
            fragment = new FragmentNotification_Broadcast_Service();
        }
        else if(id==FragmentId.NOTIFICATION){
            fragment = new FragmentNotification();
        }
        else if(id==FragmentId.BROADCASTRECEIVER){
            fragment = new FragmentBroadcastReceiver();
        }
        else if(id==FragmentId.SERVICE){
            fragment = new FragmentService();
        }

        fragment.setArguments(bundle);

        FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ts = manager.beginTransaction();
        /*ts.setCustomAnimations(R.animator.slide_in_right,
                R.animator.slide_out_left,
                R.animator.slide_in_left,
                R.animator.slide_out_right);*/

        ts.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right);

        ts.replace(R.id.contain, fragment);
        if(addToStack){
            ts.addToBackStack(id.getKey());
        }

        //
        ts.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_main, menu);

        //
        MenuItem searchItem = menu.findItem(R.id.searchItem);
        /*
        * 3.0
        * */
        //searchItem.getsearchView();

        /*
        * v7 support
        * */
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    System.out.println(newText);
                    return true;
                }
            });

            searchView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode==KeyEvent.KEYCODE_ENTER){
                        System.out.println("enter");
                    }
                    return false;
                }
            });
        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_home:
                System.out.println("item home click");
                break;
            case R.id.menu_setting:
                System.out.println("item setting click");
                break;
            case R.id.menu_product:
                System.out.println("item product click");
                break;
        }

        return false;
    }
}