package com.exam.exammodule03.fragments;

import android.view.View;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentContentProvider extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_contentprovider;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
