package com.exam.exammodule03.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by PC14-02 on 12/18/2015.
 */
public class Broadcast1 extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        //
        String text = intent.getStringExtra("data");
        System.out.println(text);
    }

}
