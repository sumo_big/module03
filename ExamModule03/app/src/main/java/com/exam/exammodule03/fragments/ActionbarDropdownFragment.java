package com.exam.exammodule03.fragments;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.exam.exammodule03.R;
import com.exam.exammodule03.adapters.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class ActionbarDropdownFragment extends BaseFragment{

    BaseFragment[] fragments = new BaseFragment[2];

    @Override
    public int getFragmentId() {
        return R.layout.fragment_actionbar_dropdown;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        //step1
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        //step2 - set adapter data
        List<String>data = new ArrayList<>();
        data.add("Danh sach hoc sinh");
        data.add("Tim kiem");

        SpinnerAdapter adapter = new SpinnerAdapter(getActivity());
        adapter.setData(data);

        actionBar.setListNavigationCallbacks(adapter, new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int position, long itemId) {
                System.out.println("Item click - " + position);

                //
                loadFragment(position);

                return false;
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    private void loadFragment(int position){

        if(fragments[position]==null) {
            if (position == 0) {
                fragments[position] = new FragmentTab1();
            } else {
                fragments[position] = new FragmentTab2();
            }
        }
        //
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        //
        ft.replace(R.id.fragment_container, fragments[position]);

        ft.commit();
    }
}
