package com.exam.exammodule03.fragments;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.exam.exammodule03.MainActivity;
import com.exam.exammodule03.R;
import com.exam.exammodule03.module.TaskDowload;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentThread extends BaseFragment{

    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            //super.handleMessage(msg);

            if (msg.what==1){
                //showToast("a");
                tv1.setText("a" + msg.obj);
            }
            else{
                //showToast("b");
                tv2.setText("b" + msg.obj);
            }

        }
    };

    TextView tv1, tv2, tv3, tv4;
    ProgressBar pbar;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_thread;
    }

    @Override
    public void initView(View view) {

        //
        View btn = view.findViewById(R.id.button);
        View btn2 = view.findViewById(R.id.button2);
        View btn5 = view.findViewById(R.id.button5);
        View btn6 = view.findViewById(R.id.button6);

        tv1 = (TextView) view.findViewById(R.id.textView1);
        tv2 = (TextView) view.findViewById(R.id.textView2);
        tv3 = (TextView) view.findViewById(R.id.textView4);
        tv4 = (TextView) view.findViewById(R.id.textView5);

        pbar = (ProgressBar) view.findViewById(R.id.progressBar);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                showToast("click");

                printA();
                printB();

                /*//b
                for (int i=0; i<1000; i++){
                    System.out.println("b"+i);
                }*/

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv3.setText("");
                tv3.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //
                        tv3.setText("123");


                    }
                }, 3000);

            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAsynctask();
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDownload();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void printA(){

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {

                //
                //a
                //for (int i=0; i<1000; i++){
                    for (int j=0; j<100; j++){
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("a" + j);

                        //tv1.setText("a"+j);

                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = j;

                        handler.sendMessage(msg);

                    }
                //}

            }
        });

        thread1.start();

    }

    private void printB(){

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {

                //
                //a
                //for (int i=0; i<1000; i++){
                    for (int j=0; j<100; j++){
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        System.out.println("b" + j);
                        //tv2.setText("b"+j);

                        /*Message msg = new Message();
                        msg.what = 2;
                        msg.obj = j;

                        handler.sendMessage(msg);*/
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //abc ....
                            tv2.setText(5+"");
                        }
                    });

                //}

            }
        });

        thread1.start();

    }

    private void startAsynctask(){

        TaskDowload ts = new TaskDowload();
        ts.url = "anc";
        ts.value1 = 100;
        ts.execute();


        AsyncTask<String, Integer, Boolean> task = new AsyncTask<String, Integer, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                System.out.println("truoc khi asyn chay");
                tv4.setTextColor(Color.RED);
            }

            @Override
            protected Boolean doInBackground(String... params) {
                System.out.println("chay...");

                String url = params[0];
                String url2 = params[1];

                int value = 3;

                for(int i=0; i<value; i++){
                    Integer[] val = new Integer[] { i };
                    this.publishProgress( val );
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                System.out.println("asyn chay xong");

                tv4.setTextColor(Color.GREEN);

                postNotify();

            }

            @Override
            protected void onProgressUpdate(Integer[] values) {
                super.onProgressUpdate(values);
                tv4.setText(values[0] + "");

                //

            }
        };
        task.execute(new String[]{"/abc.jpg", "def"});

    }

    private void postNotify(){
        //intent giong nhu tao intent de chuyen man hinh.
        Intent intent = new Intent(getActivity(), MainActivity.class);

        //truyen du lieu
        Bundle bundle = new Bundle();

        //
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //set thong tin hien thi
        Notification.Builder builder = new Notification.Builder(getActivity());
        builder.setContentTitle("title abc");
        builder.setContentText("day la noi dung thong bao");
        builder.setAutoCancel(true);
        //builder.setSound(null);
        builder.setContentIntent(pendingIntent);

        //dung de post notify len he thong
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

    }

    void startDownload(){
        //
        pbar.setMax(100);
        pbar.setProgress(50);

        AsyncTask<Integer, Integer, Boolean> ts = new AsyncTask<Integer, Integer, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(Integer... params) {

                int value = params[0];

                for (int i=0; i< value; i++){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
            }
        };

        //ts.execute( 300 );
        ts.execute(new Integer[]{200});

    }

}
