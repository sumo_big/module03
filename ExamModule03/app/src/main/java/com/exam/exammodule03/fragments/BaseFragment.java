package com.exam.exammodule03.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.exam.exammodule03.MainActivity;
import com.exam.exammodule03.constanst.FragmentId;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public abstract class BaseFragment extends Fragment{

    /*
     *
     **/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int id = getFragmentId();

        View view = inflater.inflate(id, null, false);
        initView(view);

        return view;

    }

    public abstract int getFragmentId();
    //
    public abstract void initView(View view);

    //
    public void addFragment(FragmentId id, Bundle bundle, boolean addToBackStack){
        MainActivity activity = (MainActivity) getActivity();
        activity.addFragment(id, bundle, addToBackStack);
    }

    public void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }


}
