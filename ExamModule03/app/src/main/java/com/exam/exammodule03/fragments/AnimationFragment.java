package com.exam.exammodule03.fragments;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class AnimationFragment extends BaseFragment{

    ImageView img;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_animation;
    }

    @Override
    public void initView(View view) {
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);
        View btn4 = view.findViewById(R.id.button4);
        View btn5 = view.findViewById(R.id.button5);

        img = (ImageView) view.findViewById(R.id.imageView);

        btn1.setOnClickListener(onclick);
        btn2.setOnClickListener(onclick);
        btn3.setOnClickListener(onclick);
        btn4.setOnClickListener(onclick);
        btn5.setOnClickListener(onclick);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click");
            }
        });

    }

    View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button1:
                    alpha();
                    break;
                case R.id.button2:
                    scale();
                    break;
                case R.id.button3:
                    translate();
                    break;
                case R.id.button4:
                    rotate();
                    break;
                case R.id.button5:
                    scaleRotate();
                    break;
            }
        }
    };

    private void alpha(){
        AlphaAnimation alpha = (AlphaAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.alpha);

        /*img.setAnimation(alpha);
        //alpha.start();
        alpha.startNow();*/

        img.startAnimation(alpha);

    }

    private void scale(){
        ScaleAnimation scale = (ScaleAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.scale);
        img.startAnimation(scale);
    }

    private void translate(){
        TranslateAnimation scale = (TranslateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.translate);
        img.startAnimation(scale);
    }

    private void rotate(){
        RotateAnimation scale = (RotateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        img.startAnimation(scale);
    }

    private void scaleRotate(){
        AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(getActivity(), R.anim.rotatescale);
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img.startAnimation(set);
    }
}
