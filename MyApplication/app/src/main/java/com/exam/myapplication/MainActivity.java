package com.exam.myapplication;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    String authority = "com.exam.exammodule03.provider";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        View btnList = findViewById(R.id.button1);
        View btnAdd = findViewById(R.id.button2);
        View btnSend = findViewById(R.id.button3);

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                danhsachlophoc();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                themLophoc();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //abc.def.broadcast2
                Intent intent = new Intent("abc.def.broadcast2");
                intent.putExtra("data", "hello broadcast receiver");
                MainActivity.this.sendBroadcast(intent);
            }
        });

    }

    private void danhsachlophoc(){
        //
        String lophoc = "lophoc";

        Uri uri = Uri.parse("content://" + authority + "/" + lophoc);
        Uri uri2 = Uri.parse("content://" + authority + "/" + lophoc + "/6");

        //
        Uri uri3 = Uri.parse("content://" + authority + "/" + "hocsinh");

        ContentResolver resolver = getContentResolver();
        Cursor cs = resolver.query(uri, null, null, null, null);
        if(cs!=null && cs.moveToFirst()){

            int n = cs.getCount();
            //System.out.println("count " + n);

            ArrayList<String> arr = new ArrayList<>();

            do{

                int idxML = cs.getColumnIndex("malop");
                int idxTL = cs.getColumnIndex("tenlop");
                int idxNH = cs.getColumnIndex("namhoc");

                int id = cs.getInt(idxML);
                String tenlop = cs.getString(idxTL);
                int namhoc = cs.getInt(idxNH);

                arr.add(tenlop);

                String s = id + " - " + tenlop + " - " + namhoc;
                System.out.println(s);

                //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

            }while (cs.moveToNext());

        }
    }

    private void themLophoc(){
        Random random = new Random();
        int a = random.nextInt(10) + 1;
        String tenlop = "Lop" + a;
        int namhoc = 2016;

        //c1
        /*String sql = "insert into lophoc(tenlop, namhoc) values('" + tenlop + "', " + namhoc +")";
        //insert into lophoc(tenlop, namhoc) values("lop3c", 2015);
        System.out.println("sql: " + sql);
        db.execSQL(sql);*/

        //c2
        ContentValues values = new ContentValues();
        values.put("tenlop", tenlop);
        values.put("namhoc", namhoc);

        Uri uri3 = Uri.parse("content://" + authority + "/" + "lophoc");

        ContentResolver resolver = getContentResolver();
        if(resolver.insert(uri3, values)==null){
            System.out.println("them khong thanh cong");
        }
        else {
            System.out.println("them thanh cong");
        }

    }

}
