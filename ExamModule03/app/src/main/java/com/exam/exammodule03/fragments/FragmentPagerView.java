package com.exam.exammodule03.fragments;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.exam.exammodule03.R;
import com.exam.exammodule03.adapters.PagerViewAdapter;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentPagerView extends BaseFragment{

    ViewPager viewPager;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_pagerview;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(false);

        viewPager = (ViewPager) view.findViewById(R.id.pagerView);
        viewPager.setAdapter(new PagerViewAdapter( getActivity().getSupportFragmentManager() ));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
