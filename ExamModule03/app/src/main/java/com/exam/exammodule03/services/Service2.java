package com.exam.exammodule03.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.exam.exammodule03.MainActivity;
import com.exam.exammodule03.R;

import java.util.Random;

/**
 * Created by PC14-02 on 12/18/2015.
 */
public class Service2 extends Service{

    private IBinder mBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //super.onStartCommand(intent, flags, startId);
        int max = intent.getIntExtra("value", 10);

        for(int i = 0; i<max; i++){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(""+i);
        }

        //
        postNotify(getBaseContext(), "nhan dc thong tin tu broadcast");

        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    private void postNotify(Context context, String msg){

        Random random = new Random(System.currentTimeMillis());
        int id = random.nextInt();

        //intent giong nhu tao intent de chuyen man hinh.
        Intent intent = new Intent(context, MainActivity.class);

        //truyen du lieu
        Bundle bundle = new Bundle();
        bundle.putString("data1", msg);

        intent.putExtras(bundle);

        //
        PendingIntent pendingIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //set thong tin hien thi
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle("receiver");
        builder.setContentText("noi dung receiver: " + msg);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.owl);
        builder.setSound(sound);

        //builder.setSound(null);
        builder.setContentIntent(pendingIntent);

        //dung de post notify len he thong
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(id, builder.build());

    }

    //step1
    public class MyBinder extends Binder {
        public Service2 getService() {
            return Service2.this;
        }
    }
}
