package com.exam.exammodule03.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by PC14-02 on 12/7/2015.
 */
public class MyContentProvider extends ContentProvider{

    //
    private static String authority = "com.exam.exammodule03.provider";

    private static final UriMatcher sUriMatcher;
    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(authority, HocsinhDateBase.TABLE_LOPHOC, 1);
        sUriMatcher.addURI(authority, "lophoc/#", 2);
        sUriMatcher.addURI(authority, "hocsinh", 5);
        sUriMatcher.addURI(authority, "hocsinh/#", 6);
    }

    /*
    * com.exam.exammodule03.provider/hocsinh => 5
    * com.exam.exammodule03.provider/hocsinh/ten => 6
    * */

    @Override
    public boolean onCreate() {
        HocsinhDateBase dateBase = new HocsinhDateBase(getContext());
        if(dateBase!=null)
            return true;
        return false;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        int value = sUriMatcher.match(uri);
        if(value==1)
            return "vnd.android.cursor.dir/vnd.com.exam.exammodule03.provider.lophoc";
        else
            return "vnd.android.cursor.item/vnd.com.exam.exammodule03.provider.lophoc";
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //com.exam.exammodule03.provider/hocsinh/ten
        int value = sUriMatcher.match(uri);
        if(value==1){
            //select * lophoc
            HocsinhDateBase dateBase = new HocsinhDateBase(getContext());
            SQLiteDatabase db = dateBase.getReadableDatabase();
            return db.rawQuery("select * from lophoc", null);
        }
        else if(value==2){
            String id = uri.getLastPathSegment();
            //Toast.makeText(getContext(), id, Toast.LENGTH_LONG).show();
            System.out.println(id);
            HocsinhDateBase dateBase = new HocsinhDateBase(getContext());
            SQLiteDatabase db = dateBase.getReadableDatabase();
            return db.rawQuery("select * from lophoc where malop = ?", new String[]{ id });
        }
        else if(value==5){

        }
        else if(value==6){

        }

        System.out.println("key value:" + value);

        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        HocsinhDateBase dateBase = new HocsinhDateBase(getContext());
        SQLiteDatabase db = dateBase.getWritableDatabase();
        int value = sUriMatcher.match(uri);
        switch (value){
            case 1:
            case 2:
                long id = db.insert("lophoc", null, values);
                if(id!=-1){
                    return uri;
                }
                return null;
        }

        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
