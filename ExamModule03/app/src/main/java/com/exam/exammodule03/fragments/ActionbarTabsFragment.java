package com.exam.exammodule03.fragments;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.exam.exammodule03.R;
import com.exam.exammodule03.adapters.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class ActionbarTabsFragment extends BaseFragment implements ActionBar.TabListener {

    BaseFragment fragmentTabs[] = new BaseFragment[5];

    @Override
    public int getFragmentId() {
        return R.layout.fragment_actionbar_tabs;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        //step1
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        //step2

        String tabs[] = { "Danh sach hoc sinh", "Tim kiem", "New1" , "New2" , "New3"};

        for(int i=0; i<tabs.length; i++){
            ActionBar.Tab tab = actionBar.newTab();
            tab.setText(tabs[i]);
            tab.setTabListener(this);

            //step3
            actionBar.addTab(tab);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.removeAllTabs();

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        System.out.println("tab selected - " + tab.getPosition()
        );
        loadTab(tab.getPosition(), ft);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        System.out.println("tab unselected - " + tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    private void loadTab(int postion, FragmentTransaction ft){

        if(fragmentTabs[postion]==null){
            if(postion==0){
                fragmentTabs[postion] = new FragmentTab1();
            }
            else{
                fragmentTabs[postion] = new FragmentTab2();
            }
        }

        //
        ft.replace(R.id.fragment_container, fragmentTabs[postion]);
        //ft.commit();

    }
}
