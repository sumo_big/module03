package com.exam.exammodule03.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exam.exammodule03.R;

import java.util.List;

/**
 * Created by PC14-02 on 12/4/2015.
 */
public class SpinnerAdapter extends BaseAdapter{

    private Context context;
    private List<String> data;

    public SpinnerAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        if(data==null)
            return 0;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        if(data==null)
            return null;
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        String text = (String) getItem(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.layout_spinner_item, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.textView);

        textView.setText( text );

        return view;
    }
}
