package com.exam.exammodule03.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.exam.exammodule03.R;
import com.exam.exammodule03.constanst.FragmentId;
import com.exam.exammodule03.database.HocsinhDateBase;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class MenuFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_menu;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("MenuContext");
                showMenuContext();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("MenuActionMode");
                showMenuActionMode();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Popup");

                PopupMenu popup = new PopupMenu(getActivity(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        System.out.println("popup item click");
                        return true;
                    }
                });
                popup.inflate(R.menu.menu_context2);
                popup.show();

            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        //
        menu.clear();

        inflater.inflate(R.menu.menu_fragment_menu, menu);

        //super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_context:
                System.out.println("item menucontext click");
                showMenuContext();
                break;
            case R.id.menu_action_mode:
                System.out.println("item menu actionmode click");
                showMenuActionMode();
                break;

                    }
        return true;
    }

    private void showMenuContext(){
        Bundle bundle = new Bundle();
        bundle.putString("name", "nguyen van a");
        bundle.putInt("id", 123);
        addFragment(FragmentId.MENU_FLOAT_CONTEXT, bundle, true);
    }

    private void showMenuActionMode(){
        Bundle bundle = new Bundle();
        bundle.putString("name", "nguyen van a");
        bundle.putInt("id", 123);
        addFragment(FragmentId.MENU_ACTIONMODE, bundle, true);
    }

}
