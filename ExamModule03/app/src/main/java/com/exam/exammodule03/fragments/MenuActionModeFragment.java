package com.exam.exammodule03.fragments;

import android.app.Notification;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.exam.exammodule03.R;

import java.util.ArrayList;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class MenuActionModeFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_menu_actionmode;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        View btn1 = view.findViewById(R.id.button1);
        ListView lview = (ListView) view.findViewById(R.id.listView);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().startActionMode(callback);
            }
        });

        Bundle bundle = getArguments();
        if(bundle!=null){
            System.out.println(bundle.getString("name"));
            System.out.println(bundle.getInt("id"));
        }

        //dang ky su kien long tren button


        //show listview
        ArrayList<String>data = new ArrayList<>();
        for (int i=0; i<20; i++){
            data.add("item " + i);
        }

        //
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
        lview.setAdapter(adapter);

        //
        btn1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(actionMode!=null){
                    return true;
                }
                actionMode = getActivity().startActionMode(callback);
                return true;
            }
        });

        lview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(actionMode!=null){
                    return true;
                }
                actionMode = getActivity().startActionMode(callback);
                return true;
            }
        });

    }

    ActionMode actionMode;
    ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            //MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.menu_context_delete1:
                    System.out.println("delete");
                    break;
                case R.id.menu_context_edit1:
                    System.out.println("edit");
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };

}
