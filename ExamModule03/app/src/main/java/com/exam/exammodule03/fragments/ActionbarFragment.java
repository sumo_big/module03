package com.exam.exammodule03.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.exam.exammodule03.R;
import com.exam.exammodule03.constanst.FragmentId;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class ActionbarFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_actionbar;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.ACTIONBAR_DROPDOWN, null, true);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.ACTIONBAR_TABS, null, true);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.PAGERVIEW, null, true);
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        //
        menu.clear();
        //
        inflater.inflate(R.menu.menu_actionbar, menu);

        MenuItem shareItem = menu.findItem(R.id.menu_actionprovider);
        ShareActionProvider provider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        //
        Intent myShareIntent = new Intent(Intent.ACTION_SEND);
        myShareIntent.setType("image/*");
        myShareIntent.putExtra(Intent.EXTRA_STREAM, "file:///abc.jpg");

        provider.setShareIntent(myShareIntent);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
