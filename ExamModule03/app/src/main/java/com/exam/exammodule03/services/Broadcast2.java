package com.exam.exammodule03.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import com.exam.exammodule03.MainActivity;
import com.exam.exammodule03.R;

import java.util.Random;

/**
 * Created by PC14-02 on 12/18/2015.
 */
public class Broadcast2 extends BroadcastReceiver{

    private void postNotify(Context context, String msg){

        Random random = new Random(System.currentTimeMillis());
        int id = random.nextInt();

        //intent giong nhu tao intent de chuyen man hinh.
        Intent intent = new Intent(context, MainActivity.class);

        //truyen du lieu
        Bundle bundle = new Bundle();
        bundle.putString("data1", msg);

        intent.putExtras(bundle);

        //
        PendingIntent pendingIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //set thong tin hien thi
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle("receiver");
        builder.setContentText("noi dung receiver: " + msg);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.owl);
        builder.setSound(sound);

        //builder.setSound(null);
        builder.setContentIntent(pendingIntent);

        //dung de post notify len he thong
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(id, builder.build());

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //
        String text = intent.getStringExtra("data");
        System.out.println(text);

        //postNotify(context, text);

        Intent intentService = new Intent(context, Service1.class);
        intentService.putExtra("value", 20);

        context.startService(intentService);
    }

}
