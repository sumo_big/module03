package com.exam.exammodule03.fragments;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import com.exam.exammodule03.R;
import com.exam.exammodule03.constanst.FragmentId;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentNotification_Broadcast_Service extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_not_ser_bro;
    }

    @Override
    public void initView(View view) {

        //
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.NOTIFICATION, null, true);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.BROADCASTRECEIVER, null, true);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(FragmentId.SERVICE, null, true);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


}
