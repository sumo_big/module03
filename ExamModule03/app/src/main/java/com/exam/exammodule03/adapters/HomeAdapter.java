package com.exam.exammodule03.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class HomeAdapter extends BaseAdapter{

    String[] data = new String[]{
            "Bai01-Animation",
            "Bai02-Animation(tt)",
            "Bai03-SQLite",
            "Bai04-Menu",
            "Bai05-ActionBar",
            "Bai06-ContentProvider",
            "Bai07-Thread",
            "Bai08-Notification-Broadcast-Service",
            "Bai09-"

    };

    private Context context;

    public HomeAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        view = inflater.inflate(R.layout.layout_item_main, parent, false);

        TextView tv = (TextView) view.findViewById(R.id.nameTextView);
        tv.setText("" + getItem(position).toString());
        tv.setVisibility(View.VISIBLE);

        if(position%2==0){
            view.setBackgroundColor(Color.argb(80, 99, 22, 44));
        }
        else{
            view.setBackgroundColor(Color.argb(80, 11, 22, 99));
        }

        return view;
    }
}
