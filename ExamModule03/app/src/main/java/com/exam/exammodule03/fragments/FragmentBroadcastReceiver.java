package com.exam.exammodule03.fragments;

import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;

import com.exam.exammodule03.R;
import com.exam.exammodule03.services.Broadcast1;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentBroadcastReceiver extends BaseFragment{

    Broadcast1 broadcast1;
    String key1 = "abc.def.broadcast1";

    @Override
    public int getFragmentId() {
        return R.layout.fragment_broadcast;
    }

    @Override
    public void initView(View view) {

        //
        View btn = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dang ky broadcast receiver
                IntentFilter intentFilter = new IntentFilter(key1);
                getActivity().registerReceiver(broadcast1, intentFilter);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(key1);
                intent.putExtra("data", "456");

                getActivity().sendBroadcast(intent);

            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().unregisterReceiver(broadcast1);
            }
        });

        broadcast1 = new Broadcast1();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
